// 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
// Використайте 2 способи для пошуку елементів.
// Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).

let featureElements = document.getElementsByClassName("feature");

featureElements = Array.from(featureElements);

featureElements.forEach(element => {
    element.style.textAlign = "center";
});

console.log("Знайдені елементи (за допомогою getElementsByClassName()):", featureElements);


let featureElements1 = document.querySelectorAll(".feature");

featureElements1 = Array.from(featureElements1);

featureElements1.forEach(element => {
    element.style.textAlign = "center";
});

console.log("Знайдені елементи (за допомогою querySelectorAll()):", featureElements1);


// 2. Змініть текст усіх елементів h2 на "Awesome feature".

let h2Elements = document.querySelectorAll('h2');

h2Elements.forEach(element => {
    element.textContent = "Awesome feature";
});

// 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".

let featureTitleElements = document.querySelectorAll('.feature-title');

featureTitleElements = Array.from(featureTitleElements);

featureTitleElements.forEach(element => {
    element.textContent += "!";
});